<?php


/**
* clase encargada de sincronizar la información entre salesforce y SAE
*/
class salesforceController 
{
	
	const HOST = 'C:\Archivos de Programa\Common Files\Aspel\Sistemas Aspel\SAE6.00\Ejemplos\Ejemplos.fdb';
	const USER = 'SYSDBA';
	const PASS = 'masterkey';
	const OBJECTS = 'log/';

	public function insertSftoSae()
	{

		require_once ('sfdc-php/soapclient/SforcePartnerClient.php');
		require_once ('sfdc-php/samples/userAuth.php');

		$mySforceConnection = new SforcePartnerClient();
		$mySoapClient = $mySforceConnection->createConnection("sfdc-php/soapclient/partner.wsdl.xml");
		$mylogin = $mySforceConnection->login($USERNAME, $PASSWORD);


		$colsQueries = array(
			'sae'  => 'CLAVE, STATUS, NOMBRE, RFC, CALLE, NUMINT, NUMEXT, COLONIA, CODIGO, MUNICIPIO, ESTADO, PAIS, NACIONALIDAD, REFERDIR, TIPO_EMPRESA, CLASIFIC, FAX, TELEFONO, PAG_WEB, CURP, SALDO, LIMCRED',
			'sfdc' => 'Id, KeySAE__c, Name ,RFC__c ,BillingStreet ,InteriorNumber__c ,ExternalNumber__c ,Neighborhood__c ,BillingPostalCode ,BillingCity ,BillingState, BillingCountry, ReferenceAddress__c, Clasification__c, Fax, Phone, Website, CURP__c, Balance__c, CreditLimit__c',
			'match' => array(
				'KeySAE__c'           => 'CLAVE',
				'Name'                => 'NOMBRE',
				'RFC__c'              => 'RFC',
				'BillingStreet'       => 'CALLE',
				'InteriorNumber__c'   => 'NUMINT',
				'ExternalNumber__c'   => 'NUMEXT',
				'Neighborhood__c'     => 'COLONIA',
				'BillingPostalCode'   => 'CODIGO',
				'BillingCity'         => 'MUNICIPIO',
				'BillingState'        => 'ESTADO',
				'BillingCountry'      => 'PAIS',
				'ReferenceAddress__c' => 'REFERDIR',
				'Clasification__c'    => 'CLASIFIC',
				'Fax'                 => 'FAX',
				'Phone'               => 'TELEFONO',
				'Website'             => 'PAG_WEB',
				'CURP__c'             => 'CURP',
				'Balance__c'          => 'SALDO',
				'CreditLimit__c'      => 'LIMCRED'
			)
		);


		$query = "SELECT " . $colsQueries['sfdc'] . " from Account where KeySAE__c != NULL " . self::notContenplate('sf-sae');
		

		$response = $mySforceConnection->query($query);

		$queryResult = new QueryResult($response);
		
		if ($queryResult->size == 0) {
			return 0;
		}
		
		//echo "Número de cuentas: " . $queryResult->size . "<br/>";

		$counter = 0;
		$colsInsert = array(
			'meta' => array(),
			'data' => array()
		);

		foreach ($colsQueries['match'] as $key => $value) 
			array_push($colsInsert['meta'], $value);

		$colsQueries['toInsert'] = implode(',', $colsInsert['meta']);

		$colsQueries['toData'] = array();

		foreach ($queryResult->records as $record) {

			$record = new SObject($record);

			foreach ($colsQueries['match'] as $key => $value){
				array_push($colsInsert['data'], $record->$key); 
				
				if($key == 'KeySAE__c'){
					self::addRowToJsonDefinition(
						'sf-sae',
						array(
							'url'  => 'https://na59.salesforce.com/' . $record->Id,
							'id'   => $record->Id,
							'Acc'  => $record->Name
						),
						$record->$key
					);
				}

			} 

			array_push($colsQueries['toData'], implode(',', $colsInsert['data']));

		}

		for ($i=0; $i < count($colsQueries['toData']); $i++) 
			$colsQueries['toData'][$i] = '(' . $colsQueries['toData'][$i] . ')';
		
		$finally = implode(',', $colsQueries['toData']);

		$stmt = "INSERT INTO CLIE01 (" . $colsQueries['toInsert'] . ") VALUES " . $finally; 

		// echo "<pre>";
		// echo $stmt;
		//echo $query;
		// echo "</pre>";

		// $dbh = ibase_connect(HOST, USER, PASS);
		//$stmt = "INSERT INTO CLIE01 (CLAVE, STATUS, NOMBRE, RFC) VALUES (991,'M','Axel Meneses', 123456789)";
		// $sth = ibase_query($dbh, $stmt);
		// ibase_close($dbh);



		return 1;
	}


	private static function addRowToJsonDefinition($json, $datos, $keySae){

		if(is_array($datos)){

			$pathObject = file_get_contents(self::OBJECTS . $json . '.json', FILE_USE_INCLUDE_PATH);
			$definition = json_decode($pathObject, true);

			$definition["log"]["rowsfsae"][$keySae] = $datos;

			$archivo = fopen(self::OBJECTS . $json . '.json',"w");
	    	fclose($archivo);

			//echo "definición addFieldToJsonDefinition: ";
			//print_r($definition);
			//print_r($datos[$fieldToGet]);

			$toWriteFile = fopen(self::OBJECTS . $json . '.json', "r+");
      		file_put_contents(self::OBJECTS . $json . '.json', json_encode($definition));
      		fclose($toWriteFile);
		}
	}


	private static function notContenplate($json){
		$pathObject = file_get_contents(self::OBJECTS . $json . '.json', FILE_USE_INCLUDE_PATH);
		$definition = json_decode($pathObject, true);

		$toImplode = array();
		$rImplode = '';

		if(count($definition["log"]["rowsfsae"]) == 0)
			return '';

		foreach ($definition["log"]["rowsfsae"] as $key => $value) {

			array_push($toImplode, "'" . $key . "'");
		}

		$rImplode = implode(',', $toImplode);

		return "AND KeySAE__c NOT IN ( " . $rImplode . " )";

	}

}


$cls = new salesforceController();
echo $cls->insertSftoSae();





// echo "<pre>";
// 	print_r($cls->insertSftoSae());
//echo "</pre>";

?>